# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Libraries-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import re
import os
import json
import time
import scrapy
import pandas

from datetime import datetime
from scrapy.exceptions import CloseSpider

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Class-------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


class DekalbSpider(scrapy.Spider):

    name = 'dekalb'     # To trigger crawl

    # Refer to config JSON for specs and designations
    with open('config.json') as config:
        config = json.load(config)

    # Initialize misc. vars
    date = datetime.today().strftime('%m-%d-%Y')
    label_indexes = config['label_indexes']
    startctr = 0
    batch = 1


    def __init__(self):
        """
        Generate a report for the crawl
        """

        self.generate_report()


    def generate_report(self):
        """
        Generate report
        """

        os.chdir(self.config['reports'])

        writer = pandas.ExcelWriter('Dekalb ' + self.date + '.xlsx', engine='xlsxwriter')
        workbook  = writer.book
        worksheet = workbook.add_worksheet()

        # Set column labels
        for labels in range(6):

            worksheet.write(0, labels, self.label_indexes[labels])

        writer.close()


    def start_requests(self):
        """
        Fire up url
        """

        urls = [
            self.config['url'],
        ]
        
        for url in urls:
            yield scrapy.Request(url = url, callback = self.parse)

            
    def parse(self, response):
        """
        Extract raw data, process it, and POST for new set
        """

        dataset_1 = response.xpath('//td[1]/a/text()').extract()        
        dataset_2 = response.xpath('//td[2]/text()').extract()
        dataset_3 = response.xpath('//td[3]/text()').extract()                             
        dataset_4 = response.xpath('//td[4]/text()').extract()                             
        dataset_5 = response.xpath('//td[5]/text()').extract()                             
        dataset_6 = response.xpath('//td[6]/text()').extract()
        
        data = [dataset_1, dataset_2, dataset_3, dataset_4, dataset_5, dataset_6]   # Package datasets for dataframe

        self.write_to_report(data)  # Process data

        # Additional prep for POST
        try:
            pid = response.xpath('//form/input[1]').extract()
            form_pid = re.sub(' ', '+', str(re.search('[0-9]+ [0-9]+ [0-9]+ [0-9]+', pid[0]).group(0)))

        # Recurse until prep reaches a base case of failure then terminate
        except AttributeError:
            CloseSpider

        # POST
        else:
            time.sleep(3)
            
            self.startctr += self.config['startctr']
            
            return scrapy.FormRequest.from_response(
                                            response,
                                            url = self.config['url'],
                                            formdata = {
                                                        'pid':form_pid,
                                                        'StartCtr':str(self.startctr),
                                                        'submit':'Next'
                                                       },
                                            callback = self.parse
                                            )


    def write_to_report(self, data):
        """
        Process data and update report
        """

        dlc = []

        # Process data
        for index in range(len(data)):

            mule = [re.sub('\xa0', '', parsed) for parsed in data[index] if parsed != '\r\n\t']
            transient = [parsed for parsed in mule if parsed != '\t\r\n\t']
            package = [parsed for parsed in transient if parsed not in self.label_indexes]

            if index == 1:
                auxiliary = [float(re.sub(',', '', re.sub('\$', '', parsed))) for parsed in package]
                dlc.append(auxiliary)

            else:
                dlc.append(package)
        
        print('\n')
        print('--------------------- Batch ' + str(self.batch) + ' ---------------------')
        print(dlc)
        print('--------------------- Batch ' + str(self.batch) + ' ---------------------')
        print('\n')

        # Convert data to dataframe
        inject_data = pandas.DataFrame({
                                         self.label_indexes[0]:dlc[0],
                                         self.label_indexes[1]:dlc[1],
                                         self.label_indexes[2]:dlc[2],
                                         self.label_indexes[3]:dlc[3],
                                         self.label_indexes[4]:dlc[4],
                                         self.label_indexes[5]:dlc[5]
                                        })

        # Update report with new data
        path = self.config['path'] + 'Dekalb ' + self.date + '.xlsx'
        old_dataframe = pandas.read_excel(path, 'Sheet1')

        new_dataframe = pandas.concat([old_dataframe, inject_data], sort = False)

        new_dataframe.reset_index(inplace = True, drop = True)

        new_dataframe.to_excel(path, 'Sheet1')

        self.batch += 1


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
